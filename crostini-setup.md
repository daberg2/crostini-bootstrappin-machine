# Setup Guide for Crostini
This guide is for 'bootstrapping' a fresh linux install via Crostini (primarily, this guide is oriented toward Google Pixelbooks). Additionaly, the guide includes instructions for installing/building various software that you may find handy.

## Initial Configuration
```
sudo dpkg-reconfigure tzdata
sudo apt-get install apt-file
sudo apt-file update
sudo apt-get install openjdk-8-jdk
```

### Additional packages to install
`sudo apt-get install -y git g++ libgtk-3-dev gtk-doc-tools gnutls-bin valac intltool libpcre2-dev libglib3.0-cil-dev libgnutls28-dev libgirepository1.0-dev libxml2-utils gperf`

### Make Directories
```
mkdir Downloads
mkdir bin
mkdir tmp
mkdir src
```
### Set user password
Some of the items in this guide require a password for the user. I've found it simplest to set a password that you can easily recall, rather than scour the internet in search of the default user password. This can be done by calling the `passwd` command.

`penguin% sudo passwd <USERNAME>`

Which will result in:

`Enter new UNIX password:`
Type it again, then you'll see the following if it was successful;

`passwd: password updated successfully`

### Quality Check
```
sudo apt update
apt-file find xclock
sudo apt-get install x11-apps
xclock
```
# Optional packages and software
You can run this script to expedite some of the installation process
```
curl -o crostini-pie.sh https://raw.githubusercontent.com/DictatorBob/crostini-pie/master/crostini-pie.sh
bash crostini-pie.sh
```
## Python3
```
sudo apt-get install -y python3-pip
sudo apt-get install build-essential libssl-dev libffi-dev python-dev
sudo apt-get install -y python3-venv
cd
mkdir environments
cd environments/
python3 -m venv scratch
```
## LinuxBrew
In your terminal:
`sh -c "$(curl -fsSL https://raw.githubusercontent.com/Linuxbrew/install/master/install.sh)"`
You'll likely see a warning stating that Linuxbrew isn't on your PATH. This can be resolved by running the following code in your terminal:
```
test -d ~/.linuxbrew && eval $(~/.linuxbrew/bin/brew shellenv)
test -d /home/linuxbrew/.linuxbrew && eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)
test -r ~/.bash_profile && echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.bash_profile
echo "eval \$($(brew --prefix)/bin/brew shellenv)" >>~/.profile
```


## Emacs
### Install neded packages for building from source
`sudo apt-get -y install build-essential automake texinfo libjpeg-dev libncurses5-dev`


### Clone Emacs Mirror
`git clone -b master git://git.sv.gnu.org/emacs.git`

### Build Emacs
```
cd emacs/
./autogen.sh
./configure --with-mailutils --prefix=/usr/local/stow/emacs
make
sudo make install
cd /usr/local/stow
sudo stow emacs
```

## Fish-Shell
My preferred shell, at least, at the time of writing this.

`Brew install fish`

Add oh-my-fish to manage packages and change themes.
```
git clone https://github.com/oh-my-fish/oh-my-fish
cd oh-my-fish
bin/install
```
Note: You may need to change permissions to /home/USER/.cache so that oh-my-fish can access it.



